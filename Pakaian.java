public class Pakaian {
	private int ID=0;
	private String keterangan = "-Keterangan Diperlukan-";
	private double harga=0.0;
	private int jmlStok=0;
	private static int UNIQUE_ID=0;

	public Pakaian() {
		ID=UNIQUE_ID++;
	}
	
	public int getID() {
		return ID;
	}

	public void setKeterangan (String d) {
		keterangan=d;
	}

	public String getKeterangan () {
		return keterangan;
	}

	public void setHarga(double p) {
		harga = p;
	}

	public double getHarga(){
		return harga;
	}

	public void setJmlStok(int q){
		jmlStok=q;
	}

	public int getJmlStok(){
		return jmlStok;
	}

}