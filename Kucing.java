import java.awt.Color;

public class Kucing{
	public String nama;
	public Color WarnaBulu;
	public int usia;
	public double bb;
	public boolean statusJinak;
	public String majikan;
	
	public void cetakInformasi(){
		System.out.println("Nama Kucing  = "+nama);
		System.out.println("Warna Bulu   = "+WarnaBulu);
		System.out.println("Usia Kucing  = "+usia);
		System.out.println("Berat Badan  = "+bb);
		System.out.println("Status Jinak = "+apakahJinak());
		System.out.println("Diadopsi     = "+majikan);
	}
	
	public void diadopsi(String m){
		this.majikan = m;
		this.statusJinak = true;
	}
	
	public boolean apakahJinak(){
		return statusJinak;
	}
	
	public void dilepas(){
		this.majikan = "";
		statusJinak = false;
	}
}