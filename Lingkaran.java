/**
*<h1><b>Kelas Lingkaran</b></h1>
*Kelas ini merepresentasikan Lingkaran sebagai suatu tipe data.
*Sebuah bangun datar sudut,
*Berupa himpunan titik-titik yang berjarak sama ke sebuah titik pusat
*Di bawah <i>field</i> yang dimilikinya adalah jejari, yang menyatakan jarak
*Titik-titik itu ke titik pusat.
*/
public class Lingkaran {
	float jejari;
	Lingkaran() {
		jejari = 0;
	}

	Lingkaran(float r) {
		jejari = r;
	}	
}
